new Chart(document.getElementById("line-chart"), {
    type: "line",
    data: {
      labels: ["First month", "Current month"],
      datasets: [
        {
          data: [117, 91872],
          borderColor: "#007bff",
          fill: false,
        },
      ],
    },
    options: {
      legend: {
        display: false,
      },
      responsive: true,
      maintainAspectRatio: false,
      elements: {
        line: {
          borderWidth: 8,
          tension: 0,
        },
        point: {
          radius: 7,
        },
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
            },
          },
        ],
      },
    },
  });
new Chart(document.getElementById("line-chart2"), {
    type: "line",
    data: {
      labels: ["First month", "Current month"],
      datasets: [
        {
          data: [23283, 18282528],
          borderColor: "#325c3e",
          fill: false,
        },
      ],
    },
    options: {
      legend: {
        display: false,
      },
      responsive: true,
      maintainAspectRatio: false,
      elements: {
        line: {
          borderWidth: 8,
          tension: 0,
        },
        point: {
          radius: 7,
        },
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
            },
          },
        ],
      },
    },
  });
  // Get the modal
var modal = document.getElementById("myModal");
var modal2 = document.getElementById("myModal2");
var modal3 = document.getElementById("myModal3");
var modal4 = document.getElementById("myModal4");
var modal5 = document.getElementById("myModal5");
var modal6 = document.getElementById("myModal6");
var modal7 = document.getElementById("myModal7");
var modal8 = document.getElementById("myModal8");

// Get the button that opens the modal
var totaluser = document.getElementById("totaluser");
var membership = document.getElementById("membership");
var marketplace = document.getElementById("marketplace");
var investment = document.getElementById("investment");
var investor = document.getElementById("investor");
var users = document.getElementById("users");
var resellers = document.getElementById("resellers");
var integration = document.getElementById("integration");


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
var span2 = document.getElementsByClassName("close")[1];
var span3 = document.getElementsByClassName("close")[2];
var span4 = document.getElementsByClassName("close")[3];
var span5 = document.getElementsByClassName("close")[4];
var span6 = document.getElementsByClassName("close")[5];
var span7 = document.getElementsByClassName("close")[6];
var span8 = document.getElementsByClassName("close")[7];

// When the user clicks the button, open the modal 

totaluser.onclick = function() {
  modal.style.display = "block";
}
membership.onclick = function() {
  modal2.style.display = "block";
}
marketplace.onclick = function() {
  modal3.style.display = "block";
}
investment.onclick = function() {
  modal4.style.display = "block";
}
investor.onclick = function() {
  modal5.style.display = "block";
}
users.onclick = function() {
  modal6.style.display = "block";
}
resellers.onclick = function() {
  modal7.style.display = "block";
}
integration.onclick = function() {
  modal8.style.display = "block";
}


// When the user clicks on <span> (x), close the modal
 span.onclick = function() {
   modal.style.display = "none";
}
 span2.onclick = function() {
   modal2.style.display = "none";
}
 span3.onclick = function() {
   modal3.style.display = "none";
}
 span4.onclick = function() {
   modal4.style.display = "none";
}
 span5.onclick = function() {
   modal5.style.display = "none";
}
 span6.onclick = function() {
   modal6.style.display = "none";
}
 span7.onclick = function() {
   modal7.style.display = "none";
}
 span8.onclick = function() {
   modal8.style.display = "none";
}



// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal||event.target == modal2||event.target == modal3||event.target == modal4||event.target == modal5||event.target == modal6||event.target == modal7||event.target == modal8) {
    modal.style.display = "none";
    modal2.style.display = "none";
    modal3.style.display = "none";
    modal4.style.display = "none";
    modal5.style.display = "none";
    modal6.style.display = "none";
    modal7.style.display = "none";
    modal8.style.display = "none";
  }}