new Chart(document.getElementById("budget").getContext("2d"), {
  type: "pie",
  data: {
    labels: ["Opex Spend", "Opex Budget"],
    datasets: [
      {
        backgroundColor: ["#b39640", "#6c757d"],
        data: [1600000, 1400000],
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
  },
});
new Chart(document.getElementById("covenants").getContext("2d"), {
  type: "pie",
  data: {
    labels: ["Opex Spend", "Opex Covenants"],
    datasets: [
      {
        backgroundColor: ["#b39640", "#f25e3d"],
        data: [1600000, 1400000],
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
  },
});
