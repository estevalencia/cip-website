new Chart(document.getElementById("budget").getContext("2d"), {
  type: "pie",
  data: {
    labels: ["Actual EBITDA ", "Budget EBITDA "],
    datasets: [
      {
        backgroundColor: ["#000", "#0c3ce8"],
        data: [330000, 130000],
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
  },
});
new Chart(document.getElementById("covenants").getContext("2d"), {
  type: "pie",
  data: {
    labels: ["Actual EBITDA ", "Covenants EBITDA "],
    datasets: [
      {
        backgroundColor: ["#000", "#0c3ce8"],
        data: [330000, 120000],
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
  },
});
