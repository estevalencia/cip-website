new Chart(document.getElementById("budget").getContext("2d"), {
  type: "pie",
  data: {
    labels: ["Actual Margin ", "Budget Margin "],
    datasets: [
      {
        backgroundColor: ["#28a745", "#6c757d"],
        data: [690000, 490000],
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
  },
});
new Chart(document.getElementById("covenants").getContext("2d"), {
  type: "pie",
  data: {
    labels: ["Actual Margin ", "Covenants Margin "],
    datasets: [
      {
        backgroundColor: ["#28a745", "#f25e3d"],
        data: [690000, 480000],
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
  },
});
