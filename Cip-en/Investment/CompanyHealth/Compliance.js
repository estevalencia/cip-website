new Chart(document.getElementById("cash"), {
  type: "bar",
  data: {
    labels: ["Actual Cash", "Covenant", "Above or Below"],
    datasets: [
      {
        labels: ["Actual Cash", "Covenant","Above"],
        backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f"],
        data: [3490000, 2500000, 990000],
      },
    ],
  },
  options: {
    legend: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    tooltips: {
      callbacks: {
          label: function(tooltipItem, data) {
              var dataset = data.datasets[tooltipItem.datasetIndex];
              var index = tooltipItem.index;
              return dataset.labels[index] + ': ' + dataset.data[index];
          },
      },
    },
  },
});
new Chart(document.getElementById("EBITDA"), {
  type: "bar",
  data: {
    labels: ["EBITDA", "Covenant", "Above or Below"],
    datasets: [
      {
        labels: ["EBITDA", "Covenant","Below"],
        backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f"],
        data: [2000000, 2500000, -500000],
      },
    ],
  },
  options: {
    legend: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    tooltips: {
      callbacks: {
          label: function(tooltipItem, data) {
              var dataset = data.datasets[tooltipItem.datasetIndex];
              var index = tooltipItem.index;
              return dataset.labels[index] + ': ' + dataset.data[index];
          },
      },
    },
  },
});
new Chart(document.getElementById("Margin"), {
  type: "bar",
  data: {
    labels: ["Margin", "Covenant", "Above or Below"],
    datasets: [
      {
        labels: ["Margin", "Covenant","Above"],
        backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f"],
        data: [4000000, 3800000, 200000],
      },
    ],
  },
  options: {
    legend: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    tooltips: {
      callbacks: {
          label: function(tooltipItem, data) {
              var dataset = data.datasets[tooltipItem.datasetIndex];
              var index = tooltipItem.index;
              return dataset.labels[index] + ': ' + dataset.data[index];
          },
      },
    },
  },
});
new Chart(document.getElementById("Revenue"), {
  type: "bar",
  data: {
    labels: ["Revenue", "Covenant", "Above or Below"],
    datasets: [
      {
        labels: ["Revenue Capital", "Covenant","Below"],
        backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f"],
        data: [30000000, 38000000, -8000000],
      },
    ],
  },
  options: {
    legend: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    legend: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    tooltips: {
      callbacks: {
          label: function(tooltipItem, data) {
              var dataset = data.datasets[tooltipItem.datasetIndex];
              var index = tooltipItem.index;
              return dataset.labels[index] + ': ' + dataset.data[index];
          },
      },
    },
  },
});
