function openNav() {
  document.getElementById("slider").style.width = "250px";
  document.getElementById("slider-right").style.width = "0";
  document.getElementById("bitsActive").className = "cipbits";
    document.querySelector(".dropdown-content ").style.display = "none";
}
function closeNav() {
  document.getElementById("slider").style.width = "0";
}
function openNavRight() {
  document.getElementById("slider-right").style.width = "250px";
  document.getElementById("slider").style.width = "0";
  document.getElementById("bitsActive").className = "cipbits";
    document.querySelector(".dropdown-content ").style.display = "none";
}
function closeNavRight() {
  document.getElementById("slider-right").style.width = "0";
}
function hide() {
  if (document.getElementById("slider").style.width == "250px") {
    document.getElementById("slider").style.width = "0";
  }
  if (document.getElementById("slider-right").style.width == "250px") {
    document.getElementById("slider-right").style.width = "0";
  }
  document.getElementById("bitsActive").className = "cipbits";
    document.querySelector(".dropdown-content ").style.display = "none";
}

function visible(){
  if(document.getElementById("bitsActive").className === "cipbitsActive"){
    document.getElementById("bitsActive").className = "cipbits";
    document.querySelector(".dropdown-content ").style.display = "none";
    document.getElementById("slider-right").style.width = "0";
    document.getElementById("slider").style.width = "0";
  }else
  document.getElementById("bitsActive").className = "cipbitsActive";
   document.querySelector(".dropdown-content ").style.display = "block";
   document.getElementById("slider-right").style.width = "0";
    document.getElementById("slider").style.width = "0";
}
var slideIndex = 1;
showSlides(slideIndex);

const selectElement = document.querySelector(".bits");

selectElement.addEventListener("change", (event) => {
  showChange(event.target.value);
});

function plusSlides(n) {
  showSlides((slideIndex += n));
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  if (n > slides.length) {
    slideIndex = 1;
    console.log(`entro al primero`);
  }
  if (n < 1) {
    slideIndex = slides.length;
    console.log(`entro al segundo`);
  }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slides[slideIndex - 1].style.display = "block";
  document.querySelector(".bits").value =
    slides[slideIndex - 1].attributes.id.value;
}

function showChange(x) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  for (i = 0; i < slides.length; i++) {
    if (slides[i].attributes.id.value == x) {
      showSlides((slideIndex = i + 1));
      break;
    }
  }
}