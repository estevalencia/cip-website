function openNav() {
  document.getElementById("slider").style.width = "250px";
  document.getElementById("slider-right").style.width = "0";
  document.getElementById("bitsActive").className = "cipbits";
    document.querySelector(".dropdown-content ").style.display = "none";
}
function closeNav() {
  document.getElementById("slider").style.width = "0";
}
function openNavRight() {
  document.getElementById("slider-right").style.width = "250px";
  document.getElementById("slider").style.width = "0";
  document.getElementById("bitsActive").className = "cipbits";
    document.querySelector(".dropdown-content ").style.display = "none";
}
function closeNavRight() {
  document.getElementById("slider-right").style.width = "0";
}
function hide() {
  if (document.getElementById("slider").style.width == "250px") {
    document.getElementById("slider").style.width = "0";
  }
  if (document.getElementById("slider-right").style.width == "250px") {
    document.getElementById("slider-right").style.width = "0";
  }
  document.getElementById("bitsActive").className = "cipbits";
    document.querySelector(".dropdown-content ").style.display = "none";
}
function visible(){
  if(document.getElementById("bitsActive").className === "cipbitsActive"){
    document.getElementById("bitsActive").className = "cipbits";
    document.querySelector(".dropdown-content ").style.display = "none";
    document.getElementById("slider-right").style.width = "0";
    document.getElementById("slider").style.width = "0";
  }else
  document.getElementById("bitsActive").className = "cipbitsActive";
   document.querySelector(".dropdown-content ").style.display = "block";
   document.getElementById("slider-right").style.width = "0";
    document.getElementById("slider").style.width = "0";
}
var slideIndex = 1;
showSlides(slideIndex);

const selectElement = document.querySelector(".bits");

selectElement.addEventListener("change", (event) => {
  showChange(event.target.value);
});

function plusSlides(n) {
  showSlides((slideIndex += n));
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  if (n > slides.length) {
    slideIndex = 1;
    console.log(`entro al primero`);
  }
  if (n < 1) {
    slideIndex = slides.length;
    console.log(`entro al segundo`);
  }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slides[slideIndex - 1].style.display = "block";
  document.querySelector(".bits").value =
    slides[slideIndex - 1].attributes.id.value;
}

function showChange(x) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  for (i = 0; i < slides.length; i++) {
    if (slides[i].attributes.id.value == x) {
      showSlides((slideIndex = i + 1));
      break;
    }
  }
}
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
// var btn = document.getElementById("myBtn");
var btn = document.getElementsByClassName("myBtn");

// console.log(btn);
// console.log(btn);

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
for (let index = 0; index < btn.length; index++) {
  btn[index].onclick = function() {
    modal.style.display = "block";
  }
  
}
// btn.onclick = function() {
//   modal.style.display = "block";
// }

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}