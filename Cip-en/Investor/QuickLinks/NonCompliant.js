new Chart(document.getElementById("doughnut-chart"), {
  type: "doughnut",
  data: {
    labels: [
      "Budget Revenue",
      "Actual Revenue",
      "Budget EBITDA",
      "Actual EBITDA",
      "Budget Cash",
      "Actual Cash",
    ],
    datasets: [
      {
        label: "Population (millions)",
        backgroundColor: [
          "#3e95cd",
          "#8e5ea2",
          "#3cba9f",
          "#e8c3b9",
          "#c45850",
          "#c77750",
        ],
        data: [2478, 2500, 800, 734, 584, 433],
      },
    ],
  },
  options: {},
});
