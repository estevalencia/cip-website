new Chart(document.getElementById("line-chart"), {
  type: "line",
  data: {
    labels: ["ABC", "DEF", "GHI", "JKL"],
    datasets: [
      {
        data: [32, 40, 27, 37],
        label: "Current Days",
        borderColor: "#007bff",
        fill: false,
      },
      {
        data: [16, 16, 16, 16],
        label: "Average Days To Sign",
        borderColor: "#ff8000",
        fill: false,
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
    elements: {
      line: {
        borderWidth: 8,
        tension: 0,
      },
      point: {
        radius: 7,
      },
    },
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  },
});
