new Chart(document.getElementById("line-chart"), {
  type: "line",
  data: {
    labels: ["May Revenue", "June Revenue", "July Revenue", "August Revenue"],
    datasets: [
      {
        data: [12000000, 8000000, 6000000, 5000000],
        label: "ABC Org",
        borderColor: "#007bff",
        fill: false,
      },
      {
        data: [5000000, 4000000, 3000000, 2500000],
        label: "DEF Org",
        borderColor: "#ff8000",
        fill: false,
      },
      {
        data: [3000000, 2500000, 2000000, 1000000],
        label: "JKL Org",
        borderColor: "#6c757d",
        fill: false,
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
    elements: {
      line: {
        borderWidth: 8,
        tension: 0,
      },
      point: {
        radius: 7,
      },
    },
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  },
});
