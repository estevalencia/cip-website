new Chart(document.getElementById("line-chart"), {
    type: "line",
    data: {
      labels: [2018, 2019, 2020, 2021, 2022, 2023],
      datasets: [
        {
          data: [2000000, 2100000, 2200000,2300000, 2400000, 2500000],
          label: "Revenue",
          borderColor: "#007bff",
          fill: false,
        },
        {
          data: [600000, 650000, 700000, 700000,750000, 800000],
          label: "Margin",
          borderColor: "#ff8000",
          fill: false,
        },
        {
          data: [400000, 420000, 440000, 460000,480000, 500000],
          label: "EBITDA",
          borderColor: "#6c757d",
          fill: false,
        }
      ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      elements: {
        line: {
          borderWidth: 8,
        },
        point: {
          radius: 7,
        },
      },
    },
  });
  
  function Monthly(){
    var Monthly = document.getElementById("Monthly");
    var Quarterly = document.getElementById("Quarterly");
    var Yearly = document.getElementById("Yearly");
    if (Monthly.style.display === "none") {
      Monthly.style.display = "block";
      Quarterly.style.display = "none";
      Yearly.style.display = "none";
    } 
}
  function Quarterly(){
    var Monthly = document.getElementById("Monthly");
    var Quarterly = document.getElementById("Quarterly");
    var Yearly = document.getElementById("Yearly");
    if (Quarterly.style.display === "none") {
      Quarterly.style.display = "block";
      Monthly.style.display = "none";
      Yearly.style.display = "none";
    } 
}
  function Yearly(){
    var Monthly = document.getElementById("Monthly");
    var Quarterly = document.getElementById("Quarterly");
    var Yearly = document.getElementById("Yearly");
    if (Yearly.style.display === "none") {
      Yearly.style.display = "block";
      Quarterly.style.display = "none";
      Monthly.style.display = "none";
    } 
}