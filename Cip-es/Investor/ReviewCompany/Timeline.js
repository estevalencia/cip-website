new Chart(document.getElementById("line-chart"), {
  type: "line",
  data: {
    labels: [2016, 2017, 2018, 2019, 2020],
    datasets: [
      {
        data: [2000000, 5000000, 7000000, 10000000, 15000000],
        label: "Ingresos",
        borderColor: "#007bff",
        fill: false,
      },
      {
        data: [500000, 4000000, 5000000, 8500000, 12000000],
        label: "Margen",
        borderColor: "#ff8000",
        fill: false,
      },
      {
        data: [-250000, -500000, -1000000, 1200000, 8000000],
        label: "EBITDA",
        borderColor: "#6c757d",
        fill: false,
      },
      {
        data: [-250000, -500000, -2000000, -200000, 6000000],
        label: "Flujo de efectivo",
        borderColor: "#FFFF00",
        fill: false,
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
    elements: {
      line: {
        borderWidth: 8,
      },
      point: {
        radius: 7,
      },
    },
  },
});
