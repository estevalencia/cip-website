new Chart(document.getElementById("bar-chart"), {
  type: "bar",
  data: {
    labels: ["ABC", "DEF", "GHI", "Latin JKL", "MNO"],
    datasets: [
      {
        label: "Capital solicitado",
        backgroundColor: [
          "#3e95cd",
          "#8e5ea2",
          "#3cba9f",
          "#e8c3b9",
          "#c45850",
        ],
        data: [2500000, 10000000, 40000000, 10000000, 4000000],
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  },
});
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
// var btn = document.getElementById("myBtn");
var btn = document.getElementById("myBtn");


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
// for (let index = 0; index < btn.length; index++) {
//   btn[index].onclick = function() {
//     modal.style.display = "block";
//   }
  
// }
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
 span.onclick = function() {
   modal.style.display = "none";
}



// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal ) {
    modal.style.display = "none";
  }}