function Document(){
    var Document = document.getElementById("Document");
    var Credit = document.getElementById("Credit");
    var Proceeds = document.getElementById("Proceeds");
    var Revenue = document.getElementById("Revenue");
    var Margin = document.getElementById("Margin");
    var EBITDA = document.getElementById("EBITDA");
    var Cash = document.getElementById("Cash");
    var Opex = document.getElementById("Opex");
    var Lawsuits = document.getElementById("Lawsuits");
    var CF = document.getElementById("CF");
    var Account = document.getElementById("Account");
    var KP = document.getElementById("KP");
    if (Document.style.display === "none") {
        Document.style.display = "block";
        Credit.style.display = "none";
        Proceeds.style.display = "none";
        Revenue.style.display = "none";
        Margin.style.display = "none";
        EBITDA.style.display = "none";
        Cash.style.display = "none";
        Opex.style.display = "none";
        Lawsuits.style.display = "none";
        CF.style.display = "none";
        KP.style.display = "none";
        Account.style.display = "none";

    } 
}
function Credit(){
    var Document = document.getElementById("Document");
    var Credit = document.getElementById("Credit");
    var Proceeds = document.getElementById("Proceeds");
    var Revenue = document.getElementById("Revenue");
    var Margin = document.getElementById("Margin");
    var EBITDA = document.getElementById("EBITDA");
    var Cash = document.getElementById("Cash");
    var Opex = document.getElementById("Opex");
    var Lawsuits = document.getElementById("Lawsuits");
    var CF = document.getElementById("CF");
    var Account = document.getElementById("Account");
    var KP = document.getElementById("KP");
    if (Credit.style.display === "none") {
        Credit.style.display = "grid";
        Document.style.display = "none";
        Proceeds.style.display = "none";
        Revenue.style.display = "none";
        Margin.style.display = "none";
        EBITDA.style.display = "none";
        Cash.style.display = "none";
        Opex.style.display = "none";
        Lawsuits.style.display = "none";
        CF.style.display = "none";
        KP.style.display = "none";
        Account.style.display = "none";

    } 
}
function Proceeds(){
    var Document = document.getElementById("Document");
    var Credit = document.getElementById("Credit");
    var Proceeds = document.getElementById("Proceeds");
    var Revenue = document.getElementById("Revenue");
    var Margin = document.getElementById("Margin");
    var EBITDA = document.getElementById("EBITDA");
    var Cash = document.getElementById("Cash");
    var Opex = document.getElementById("Opex");
    var Lawsuits = document.getElementById("Lawsuits");
    var CF = document.getElementById("CF");
    var Account = document.getElementById("Account");
    var KP = document.getElementById("KP");
    if (Proceeds.style.display === "none") {
        Proceeds.style.display = "grid";
        Document.style.display = "none";
        Credit.style.display = "none";
        Revenue.style.display = "none";
        Margin.style.display = "none";
        EBITDA.style.display = "none";
        Cash.style.display = "none";
        Opex.style.display = "none";
        Lawsuits.style.display = "none";
        CF.style.display = "none";
        KP.style.display = "none";
        Account.style.display = "none";

    } 
}
function Revenue(){
    var Document = document.getElementById("Document");
    var Credit = document.getElementById("Credit");
    var Proceeds = document.getElementById("Proceeds");
    var Revenue = document.getElementById("Revenue");
    var Margin = document.getElementById("Margin");
    var EBITDA = document.getElementById("EBITDA");
    var Cash = document.getElementById("Cash");
    var Opex = document.getElementById("Opex");
    var Lawsuits = document.getElementById("Lawsuits");
    var CF = document.getElementById("CF");
    var Account = document.getElementById("Account");
    var KP = document.getElementById("KP");
    if (Revenue.style.display === "none") {
        Revenue.style.display = "grid";
        Document.style.display = "none";
        Credit.style.display = "none";
        Proceeds.style.display = "none";
        Margin.style.display = "none";
        EBITDA.style.display = "none";
        Cash.style.display = "none";
        Opex.style.display = "none";
        Lawsuits.style.display = "none";
        CF.style.display = "none";
        KP.style.display = "none";
        Account.style.display = "none";

    } 
}
function Margin(){
    var Document = document.getElementById("Document");
    var Credit = document.getElementById("Credit");
    var Proceeds = document.getElementById("Proceeds");
    var Revenue = document.getElementById("Revenue");
    var Margin = document.getElementById("Margin");
    var EBITDA = document.getElementById("EBITDA");
    var Cash = document.getElementById("Cash");
    var Opex = document.getElementById("Opex");
    var Lawsuits = document.getElementById("Lawsuits");
    var CF = document.getElementById("CF");
    var Account = document.getElementById("Account");
    var KP = document.getElementById("KP");
    if (Margin.style.display === "none") {
        Margin.style.display = "grid";
        Document.style.display = "none";
        Credit.style.display = "none";
        Proceeds.style.display = "none";
        Revenue.style.display = "none";
        EBITDA.style.display = "none";
        Cash.style.display = "none";
        Opex.style.display = "none";
        Lawsuits.style.display = "none";
        CF.style.display = "none";
        KP.style.display = "none";
        Account.style.display = "none";

    } 
}
function EBITDA(){
    var Document = document.getElementById("Document");
    var Credit = document.getElementById("Credit");
    var Proceeds = document.getElementById("Proceeds");
    var Revenue = document.getElementById("Revenue");
    var Margin = document.getElementById("Margin");
    var EBITDA = document.getElementById("EBITDA");
    var Cash = document.getElementById("Cash");
    var Opex = document.getElementById("Opex");
    var Lawsuits = document.getElementById("Lawsuits");
    var CF = document.getElementById("CF");
    var Account = document.getElementById("Account");
    var KP = document.getElementById("KP");
    if (EBITDA.style.display === "none") {
        EBITDA.style.display = "grid";
        Document.style.display = "none";
        Credit.style.display = "none";
        Proceeds.style.display = "none";
        Revenue.style.display = "none";
        Margin.style.display = "none";
        Cash.style.display = "none";
        Opex.style.display = "none";
        Lawsuits.style.display = "none";
        CF.style.display = "none";
        KP.style.display = "none";
        Account.style.display = "none";

    } 
}
function Cash(){
    var Document = document.getElementById("Document");
    var Credit = document.getElementById("Credit");
    var Proceeds = document.getElementById("Proceeds");
    var Revenue = document.getElementById("Revenue");
    var Margin = document.getElementById("Margin");
    var EBITDA = document.getElementById("EBITDA");
    var Cash = document.getElementById("Cash");
    var Opex = document.getElementById("Opex");
    var Lawsuits = document.getElementById("Lawsuits");
    var CF = document.getElementById("CF");
    var Account = document.getElementById("Account");
    var KP = document.getElementById("KP");
    if (Cash.style.display === "none") {
        Cash.style.display = "grid";
        Document.style.display = "none";
        Credit.style.display = "none";
        Proceeds.style.display = "none";
        Revenue.style.display = "none";
        Margin.style.display = "none";
        EBITDA.style.display = "none";
        Opex.style.display = "none";
        Lawsuits.style.display = "none";
        CF.style.display = "none";
        KP.style.display = "none";
        Account.style.display = "none";

    } 
}
function Opex(){
    var Document = document.getElementById("Document");
    var Credit = document.getElementById("Credit");
    var Proceeds = document.getElementById("Proceeds");
    var Revenue = document.getElementById("Revenue");
    var Margin = document.getElementById("Margin");
    var EBITDA = document.getElementById("EBITDA");
    var Cash = document.getElementById("Cash");
    var Opex = document.getElementById("Opex");
    var Lawsuits = document.getElementById("Lawsuits");
    var CF = document.getElementById("CF");
    var Account = document.getElementById("Account");
    var KP = document.getElementById("KP");
    if (Opex.style.display === "none") {
        Opex.style.display = "grid";
        Document.style.display = "none";
        Credit.style.display = "none";
        Proceeds.style.display = "none";
        Revenue.style.display = "none";
        Margin.style.display = "none";
        EBITDA.style.display = "none";
        Cash.style.display = "none";
        Lawsuits.style.display = "none";
        CF.style.display = "none";
        KP.style.display = "none";
        Account.style.display = "none";

    } 
}
function Lawsuits(){
    var Document = document.getElementById("Document");
    var Credit = document.getElementById("Credit");
    var Proceeds = document.getElementById("Proceeds");
    var Revenue = document.getElementById("Revenue");
    var Margin = document.getElementById("Margin");
    var EBITDA = document.getElementById("EBITDA");
    var Cash = document.getElementById("Cash");
    var Opex = document.getElementById("Opex");
    var Lawsuits = document.getElementById("Lawsuits");
    var CF = document.getElementById("CF");
    var Account = document.getElementById("Account");
    var KP = document.getElementById("KP");
    if (Lawsuits.style.display === "none") {
        Lawsuits.style.display = "grid";
        Document.style.display = "none";
        Credit.style.display = "none";
        Proceeds.style.display = "none";
        Revenue.style.display = "none";
        Margin.style.display = "none";
        EBITDA.style.display = "none";
        Cash.style.display = "none";
        Opex.style.display = "none";
        CF.style.display = "none";
        KP.style.display = "none";
        Account.style.display = "none";

    } 
}
function CF(){
    var Document = document.getElementById("Document");
    var Credit = document.getElementById("Credit");
    var Proceeds = document.getElementById("Proceeds");
    var Revenue = document.getElementById("Revenue");
    var Margin = document.getElementById("Margin");
    var EBITDA = document.getElementById("EBITDA");
    var Cash = document.getElementById("Cash");
    var Opex = document.getElementById("Opex");
    var Lawsuits = document.getElementById("Lawsuits");
    var CF = document.getElementById("CF");
    var Account = document.getElementById("Account");
    var KP = document.getElementById("KP");
    if (CF.style.display === "none") {
        CF.style.display = "grid";
        Document.style.display = "none";
        Credit.style.display = "none";
        Proceeds.style.display = "none";
        Revenue.style.display = "none";
        Margin.style.display = "none";
        EBITDA.style.display = "none";
        Cash.style.display = "none";
        Opex.style.display = "none";
        Lawsuits.style.display = "none";
        KP.style.display = "none";
        Account.style.display = "none";

    } 
}
function Account(){
    var Document = document.getElementById("Document");
    var Credit = document.getElementById("Credit");
    var Proceeds = document.getElementById("Proceeds");
    var Revenue = document.getElementById("Revenue");
    var Margin = document.getElementById("Margin");
    var EBITDA = document.getElementById("EBITDA");
    var Cash = document.getElementById("Cash");
    var Opex = document.getElementById("Opex");
    var Lawsuits = document.getElementById("Lawsuits");
    var CF = document.getElementById("CF");
    var Account = document.getElementById("Account");
    var KP = document.getElementById("KP");
    if (Account.style.display === "none") {
        Account.style.display = "grid";
        Document.style.display = "none";
        Credit.style.display = "none";
        Proceeds.style.display = "none";
        Revenue.style.display = "none";
        Margin.style.display = "none";
        EBITDA.style.display = "none";
        Cash.style.display = "none";
        Opex.style.display = "none";
        Lawsuits.style.display = "none";
        KP.style.display = "none";
        CF.style.display = "none";

    }
}
function KP(){
    var Document = document.getElementById("Document");
    var Credit = document.getElementById("Credit");
    var Proceeds = document.getElementById("Proceeds");
    var Revenue = document.getElementById("Revenue");
    var Margin = document.getElementById("Margin");
    var EBITDA = document.getElementById("EBITDA");
    var Cash = document.getElementById("Cash");
    var Opex = document.getElementById("Opex");
    var Lawsuits = document.getElementById("Lawsuits");
    var CF = document.getElementById("CF");
    var Account = document.getElementById("Account");
    var KP = document.getElementById("KP");
    if (KP.style.display === "none") {
        KP.style.display = "grid";
        Document.style.display = "none";
        Credit.style.display = "none";
        Proceeds.style.display = "none";
        Revenue.style.display = "none";
        Margin.style.display = "none";
        EBITDA.style.display = "none";
        Cash.style.display = "none";
        Opex.style.display = "none";
        Lawsuits.style.display = "none";
        Account.style.display = "none";
        CF.style.display = "none";

    } 
}