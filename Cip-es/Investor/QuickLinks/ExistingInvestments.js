new Chart(document.getElementById("bar-chart-grouped"), {
  type: "bar",
  data: {
    labels: ["ABC", "DEF", "GHI", "JKL", "MNO", "PQR", "STU", "VWX", "AAB"],
    datasets: [
      {
        label: "Ingresos reales",
        backgroundColor: "#3e95cd",
        data: [
          4000000,
          5000000,
          6000000,
          7000000,
          1000000,
          2000000,
          3000000,
          5000000,
          7000000,
        ],
      },
      {
        label: "Presupuesto de ingresos",
        backgroundColor: "#001f41",
        data: [
          2000000,
          3000000,
          3000000,
          6000000,
          3000000,
          3000000,
          5000000,
          10000000,
          8000000,
        ],
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  },
});
