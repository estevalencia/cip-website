new Chart(document.getElementById("doughnut-chart"), {
  type: "doughnut",
  data: {
    labels: [
      "Ingresos del presupuesto",
      "Ingresos reales",
      "Presupuesto EBITDA",
      "Actual EBITDA",
      "Presupuesto en efectivo",
      "efectivo",
    ],
    datasets: [
      {
        backgroundColor: [
          "#3e95cd",
          "#8e5ea2",
          "#3cba9f",
          "#e8c3b9",
          "#c45850",
          "#c77750",
        ],
        data: [2478, 2500, 800, 734, 584, 433],
      },
    ],
  },
  options: {},
});
