new Chart(document.getElementById("budget").getContext("2d"), {
  type: "pie",
  data: {
    labels: ["Gastos de Operación", "Presupuesto para Gastos de Operación"],
    datasets: [
      {
        backgroundColor: ["#b39640", "#6c757d"],
        data: [1600000, 1400000],
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
  },
});
new Chart(document.getElementById("covenants").getContext("2d"), {
  type: "pie",
  data: {
    labels: ["Gastos de Operación", "Pactos de Gastos de Operación"],
    datasets: [
      {
        backgroundColor: ["#b39640", "#f25e3d"],
        data: [1600000, 1400000],
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
  },
});
