new Chart(document.getElementById("budget").getContext("2d"), {
  type: "pie",
  data: {
    labels: ["Actual Cash", "Budget Cash"],
    datasets: [
      {
        backgroundColor: ["#28a745", "#f25e3d"],
        data: [3300000, 2200000],
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
  },
});
new Chart(document.getElementById("covenants").getContext("2d"), {
  type: "pie",
  data: {
    labels: ["Actual Cash", "Covenants Cash"],
    datasets: [
      {
        backgroundColor: ["#28a745", "#cf3476"],
        data: [2078000, 2200000],
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
  },
});
