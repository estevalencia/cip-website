new Chart(document.getElementById("budget").getContext("2d"), {
  type: "pie",
  data: {
    labels: ["Ingresos actuales ", "Ingresos del presupuesto "],
    datasets: [
      {
        backgroundColor: ["#28a745", "#007bff"],
        data: [2300000, 2100000],
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
  },
});
new Chart(document.getElementById("covenants").getContext("2d"), {
  type: "pie",
  data: {
    labels: ["Ingresos Actuales ", "Ingresos Pactados "],
    datasets: [
      {
        backgroundColor: ["#28a745", "#007bff"],
        data: [2300000, 2090000],
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
  },
});
