new Chart(document.getElementById("cash"), {
  type: "bar",
  data: {
    labels: ["Efectivo Actual", "Pacto", "Por encima o por debajo"],
    datasets: [
      {
        labels: ["Efectivo Actual", "Pacto","Por encima"],
        backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f"],
        data: [3490000, 2500000, 990000],
      },
    ],
  },
  options: {
    legend: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    tooltips: {
      callbacks: {
          label: function(tooltipItem, data) {
              var dataset = data.datasets[tooltipItem.datasetIndex];
              var index = tooltipItem.index;
              return dataset.labels[index] + ': ' + dataset.data[index];
          },
      },
    },
  },
});
new Chart(document.getElementById("EBITDA"), {
  type: "bar",
  data: {
    labels: ["EBITDA", "Pacto", "Por encima o por debajo"],
    datasets: [
      {
        labels: ["EBITDA", "Pacto","Por debajo"],
        backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f"],
        data: [2000000, 2500000, -500000],
      },
    ],
  },
  options: {
    legend: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    tooltips: {
      callbacks: {
          label: function(tooltipItem, data) {
              var dataset = data.datasets[tooltipItem.datasetIndex];
              var index = tooltipItem.index;
              return dataset.labels[index] + ': ' + dataset.data[index];
          },
      },
    },
  },
});
new Chart(document.getElementById("Margin"), {
  type: "bar",
  data: {
    labels: ["Margen", "Pacto", "Por encima o por debajo"],
    datasets: [
      {
        labels: ["Margen", "Pacto","Por encima"],
        backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f"],
        data: [4000000, 3800000, 200000],
      },
    ],
  },
  options: {
    legend: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    tooltips: {
      callbacks: {
          label: function(tooltipItem, data) {
              var dataset = data.datasets[tooltipItem.datasetIndex];
              var index = tooltipItem.index;
              return dataset.labels[index] + ': ' + dataset.data[index];
          },
      },
    },
  },
});
new Chart(document.getElementById("Revenue"), {
  type: "bar",
  data: {
    labels: ["Ingresos", "Pacto", "Por encima o por debajo"],
    datasets: [
      {
        labels: ["Ingresos de Capital", "Pacto","Por debajo"],
        backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f"],
        data: [30000000, 38000000, -8000000],
      },
    ],
  },
  options: {
    legend: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    legend: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    tooltips: {
      callbacks: {
          label: function(tooltipItem, data) {
              var dataset = data.datasets[tooltipItem.datasetIndex];
              var index = tooltipItem.index;
              return dataset.labels[index] + ': ' + dataset.data[index];
          },
      },
    },
  },
});
