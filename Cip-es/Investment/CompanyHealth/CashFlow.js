new Chart(document.getElementById("line-chart"), {
  type: "line",
  data: {
    labels: ["Dinero Neto Actual", "Dinero Neto Previsto"],
    datasets: [
      {
        data: [3500000, 2750000],
        borderColor: "#007bff",
        fill: false,
      },
    ],
  },
  options: {
    legend: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    elements: {
      line: {
        borderWidth: 8,
        tension: 0,
      },
      point: {
        radius: 7,
      },
    },
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  },
});
new Chart(document.getElementById("bar-chart"), {
  type: "bar",
  data: {
    labels: ["Actual Net Cash", "Net Cash Forecast", "Net Cash Chenge"],
    datasets: [
      {
        backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f"],
        data: [3490000, 2800000, 690000],
      },
    ],
  },
  options: {
    legend: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  },
});
