function business(){
    var capital = document.getElementById("capital");
    var business = document.getElementById("business");
    if (business.style.display === "none" ) {
        business.style.display = "grid";
        business.className += " animation";
        capital.style.display = "none";
    } 
}
function capital(){
    var capital = document.getElementById("capital");
    var business = document.getElementById("business");
    var btnbusiness = document.getElementById("btn-business");
    var btncapital = document.getElementById("btn-capital");
    if (capital.style.display === "none" ) {
        capital.style.display = "grid";
        btnbusiness.style.backgroundColor = "#c1c6c9"
        btncapital.style.backgroundColor = "#a9aeb1"
        capital.className += " animation";
        business.style.display = "none";
    } 
}
// Get the modal
var modal = document.getElementById("myModal");
var modal1 = document.getElementById("myModal1");
var btn = document.getElementById("myBtn");
var btn1 = document.getElementById("myBtn1");
var span = document.getElementsByClassName("close")[0];
var span1 = document.getElementsByClassName("close")[1];
btn.onclick = function() {
  modal.style.display = "block";
}
btn1.onclick = function() {
  modal1.style.display = "block";
}
 span.onclick = function() {
   modal.style.display = "none";
}
 span1.onclick = function() {
   modal1.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal || event.target == modal1) {
    modal.style.display = "none";
    modal1.style.display = "none";
  }
}

function myFunction() {
  var x = document.getElementById("moves");
  if ( x.style.display === "none") {
    x.style.display = "inline";
  } else {
    x.style.display = "none";
  }
}

function capitalsection() {
  var tilt = document.getElementById("tilt");
  tilt.style.opacity = 0;
  tilt.style.transform = "scale(0)";
  setTimeout(function() {
    tilt.style.display = "none";
  },750);
  var card = document.getElementsByClassName("c-capital");
  for (let index = 0; index < card.length; index++) {
    card[index].style.transform = "scale(0)";
    card[index].className = "card c-capital";
  }
  var capital = document.getElementById("capital");
  if (capital.style.display === "none" ) {
  setTimeout(function() {
    capital.style.display = "grid";
  },750);
  capital.style.opacity = 1;
  capital.style.transform = "scale(1)";
  }
  for (let index = 0; index < card.length; index++) {
    setTimeout(function() {
      card[index].className += " scale";
    },750*(index +1));
  }
  for (let index = 0; index < card.length; index++) {
    setTimeout(function() {
      card[index].style = "";
    },750*(index +1));
  }
}
function businesssection() {
  var tilt = document.getElementById("tilt");
  tilt.style.opacity = 0;
  tilt.style.transform = "scale(0)";
  setTimeout(function() {
    tilt.style.display = "none";
  },750);
  var card = document.getElementsByClassName("c-business");
  for (let index = 0; index < card.length; index++) {
    card[index].style.transform = "scale(0)";
    card[index].className = "card c-business";
  }
  var business = document.getElementById("business");
  if (business.style.display === "none" ) {
  setTimeout(function() {
    business.style.display = "grid";
  },750);
  business.style.opacity = 1;
  business.style.transform = "scale(1)";
  }
  for (let index = 0; index < card.length; index++) {
    setTimeout(function() {
      card[index].className += " scale";
    },750*(index +1));
  }
  for (let index = 0; index < card.length; index++) {
    setTimeout(function() {
      card[index].style = "";
    },750*(index +1));
  }
}

function back() {
  var tilt = document.getElementById("tilt");
  var capital = document.getElementById("capital");
  var business = document.getElementById("business");
  business.style.opacity = 0;
  business.style.transform = "scale(0)";
  capital.style.opacity = 0;
  capital.style.transform = "scale(0)";
  setTimeout(function() {
    business.style.display = "none";
    capital.style.display = "none";
    business.className = "conteiner";
    capital.className = "conteiner";
  },750);
  setTimeout(function() {
    tilt.style.display = "block";
  },500);
  setTimeout(function() {
    tilt.style.opacity = 1;
  tilt.style.transform = "scale(1)";
  },750);
}

function scrolltop() {
  var back = document.getElementsByClassName("back")[0];
  var scrolltop = document.documentElement.scrollTop;
   if (scrolltop > document.getElementsByClassName("title")[0].offsetTop - 250) {
     back.style = "";
    }else{
    back.style.position = "relative";
  }
}

window.addEventListener('scroll', scrolltop)

let el = document.getElementById('logo1')
const height = el.clientHeight
const width = el.clientWidth
el.addEventListener('mousemove', handleMove)
function handleMove(e) {
  const xVal = e.layerX
  const yVal = e.layerY
  const yRotation = 20 * ((xVal - width / 2) / width)
  const xRotation = -20 * ((yVal - height / 2) / height)
  const string = 'perspective(500px) scale(1.1) rotateX(' + xRotation + 'deg) rotateY(' + yRotation + 'deg)'
  el.style.transform = string
}
el.addEventListener('mouseout', function() {
  el.style.transform = 'perspective(500px) scale(1) rotateX(0) rotateY(0)'
})

el.addEventListener('mousedown', function() {
  el.style.transform = 'perspective(500px) scale(0.9) rotateX(0) rotateY(0)'
})

el.addEventListener('mouseup', function() {
  el.style.transform = 'perspective(500px) scale(1.1) rotateX(0) rotateY(0)'
})
let ex = document.getElementById('logo2')
const heightx = ex.clientHeight
const widthx = ex.clientWidth
ex.addEventListener('mousemove', handleMoves)
function handleMoves(x) {
  const xVal = x.layerX
  const yVal = x.layerY
  const yRotation = 20 * ((xVal - widthx / 2) / widthx)
  const xRotation = -20 * ((yVal - heightx / 2) / heightx)
  const string = 'perspective(500px) scale(1.1) rotateX(' + xRotation + 'deg) rotateY(' + yRotation + 'deg)'
  ex.style.transform = string
}
ex.addEventListener('mouseout', function() {
  ex.style.transform = 'perspective(500px) scale(1) rotateX(0) rotateY(0)'
})

ex.addEventListener('mousedown', function() {
  ex.style.transform = 'perspective(500px) scale(0.9) rotateX(0) rotateY(0)'
})

ex.addEventListener('mouseup', function() {
  ex.style.transform = 'perspective(500px) scale(1.1) rotateX(0) rotateY(0)'
})
